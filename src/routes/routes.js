import HomePage from '../containers/HomePage';
import ArticleList from '../containers/ArticleList';
import ArticlePage from '../containers/ArticlePage';
import ProfilePage from '../containers/ProfilePage';

const routes = [
  {
    name: 'beranda',
    text: 'Beranda',
    path: '/',
    dropdown: false,
    component: HomePage,
  },
  {
    name: 'profil',
    text: 'Profil',
    path: [
      '/profile',
      '/profile/:slug',
    ],
    dropdown: [
      {
        name: 'kejaksaan-negeri-tanah-bumbu',
        text: 'Kejaksaan Negeri Tanah Bumbu',
        path: '/profile/kejaksaan-negeri-tanah-bumbu',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'visi-misi',
        text: 'Visi &  Misi',
        path: '/profile/visi-misi',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'tri-krama-adhyaksa',
        text: 'Tri Krama Adhyaksa',
        path: '/profile/tri-krama-adhyaksa',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'struktur-organisasi',
        text: 'Struktur Organisasi',
        path: '/profile/struktur-organisasi',
        dropdown: false,
        component: ProfilePage,
      },
    ],
    component: ProfilePage,
  },

  {
    name: 'organisasi',
    text: 'Organisasi',
    path: [
      '/organisasi',
      '/organisasi/:slug'
    ],
    dropdown: [
      {
        name: 'pidana',
        text: 'Pidana',
        path: '/organisasi/pidana',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'perdata',
        text: 'Perdata',
        path: '/organisasi/perdata',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'tri-krama-adhyaksa',
        text: 'Tri Krama Adhyaksa',
        path: '/profile/tri-krama-adhyaksa',
        dropdown: false,
        component: ProfilePage,
      },
    ],
    component: HomePage,
  },

  {
    name: 'sarana',
    text: 'Sarana',
    path: '/sarana',
    dropdown: [
      {
        name: 'pos-pelayanan-hukum',
        text: 'Pos Pelayanan Hukum',
        path: '/profile/pos-pelayanan-hukum',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'musholla',
        text: 'Musholla',
        path: '/profile/musholla',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'tk-adhyaksa',
        text: 'TK Adhyaksa',
        path: '/profile/tk-adhyaksa',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'tk-adhyaksa',
        text: 'TK Adhyaksa',
        path: '/profile/tk-adhyaksa',
        dropdown: false,
        component: ProfilePage,
      },

      {
        name: 'lapangan-futsal',
        text: 'Lapangan Futsal',
        path: '/profile/lapangan-futsal',
        dropdown: false,
        component: ProfilePage,
      },

      {
        name: 'lapangan-tenis',
        text: 'Lapangan Tenis',
        path: '/profile/lapangan-tenis',
        dropdown: false,
        component: ProfilePage,
      },

      {
        name: 'lapangan-basket',
        text: 'Lapangan Basket',
        path: '/profile/lapangan-basket',
        dropdown: false,
        component: ProfilePage,
      },
      {
        name: 'tilang',
        text: 'Tilang',
        path: '/profile/tilang',
        dropdown: false,
        component: ProfilePage,
      },
    ],
    component: HomePage,
  },
  {
    name: 'peraturan',
    text: 'Peraturan',
    path: '/peraturan',
    dropdown: false,
    component: HomePage,
  },
  {
    name: 'galeri',
    text: 'Galeri',
    path: '/galeri',
    dropdown: false,
    component: HomePage,
  },

  {
    name: 'barang-bukti',
    text: 'Barang Bukti',
    path: '/barang-bukti',
    dropdown: false,
    component: HomePage,
  },
  {
    name: 'info-terkini',
    text: 'Info Terkini',
    path: '/info-terkini',
    dropdown: false,
    component: ArticleList,
  },
  {
    name: 'info',
    text: 'Info',
    path: '/info',
    dropdown: false,
    component: ArticlePage,
  },
];

export default routes;
