import React from 'react';
import { Route } from 'react-router-dom';

import routes from './adminRoutes';

const AdminRoutes = () => (
    <div>
      {
        routes.map(
          route => <Route
            exact
            key={route.name}
            component={route.component}
            path={route.path}
          />
        )
      }
    </div>
);

export default AdminRoutes;