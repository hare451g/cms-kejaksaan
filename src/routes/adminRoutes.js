import AdminPage from "../containers/AdminPage";

const routes = [
  {
    name: 'dashboard',
    text: 'Dashboard',
    path: '/admin/',
    dropdown: false,
    component: AdminPage,
    icon: 'computer'
  },
  {
    name: 'article-management',
    text: 'Articles',
    path: '/admin/articles',
    dropdown: false,
    component: AdminPage,
    icon: 'newspaper outline',
  },
  {
    name: 'user-management',
    text: 'Users',
    path: '/admin/users',
    dropdown: false,
    component: AdminPage,
    icon: 'users',
  },
  {
    name: 'settings',
    text: 'Website Setting',
    path: '/admin/settings',
    dropdown: false,
    component: AdminPage,
    icon: 'settings',
  },
];

export default routes;
