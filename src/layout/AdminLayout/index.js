import React from 'react';
import {
  Grid, Image,
} from 'semantic-ui-react';
// components
import DashboardSideMenu from '../../components/DashboardSideMenu';
import logo from '../../assets/images/logo-kejaksaan.png';

const AdminLayout = ({
  user,
  routes,
  adminRoutes,
  activeRoute,
  changeActiveRoute,
}) => (
  <div>
    <Grid columns="equals">
      <Grid.Column width={3}>
        <Image src={logo}/>
        <DashboardSideMenu
          activeRoute={activeRoute}
          routes={adminRoutes}
          onChangeRoute={changeActiveRoute}
        />
      </Grid.Column>
      <Grid.Column width={12}>
        {routes}
      </Grid.Column>
    </Grid>
  </div>
);

export default AdminLayout;