import React from 'react';
import PropTypes from 'prop-types';

import {
  Grid,
  Container,
  Divider,
  Input,
  Image,
} from 'semantic-ui-react';

// types
import articleType from '../../types/articleType';

import logo from '../../assets/images/logo-kejaksaan.png';

const ArticleListLayout = ({ articles }) => (
  <Container>
    <Grid>
      <Grid.Column>
        <h1 className="display-title">Cari Artikel</h1>
        <Input
          size="massive"
          icon={{ name: "search", circular: true, link: true }}
          placeholder="Judul, penulis, divisi . . ."
          fluid
        />
        <h5 align="right">Hasil Pencarian: {articles.length} artikel</h5>
      </Grid.Column>
    </Grid>
    <Grid centered>
      <Grid.Column width={16}>
        {articles}
      </Grid.Column>
    </Grid>
  </Container>
);

ArticleListLayout.PropTypes = {
  article: PropTypes.shape(articleType).isRequired,
};

export default ArticleListLayout;