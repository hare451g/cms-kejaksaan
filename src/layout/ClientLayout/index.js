import React from 'react';
import {
  Grid, Image,
} from 'semantic-ui-react';
// components
import NavigationBar from '../../components/NavigationBar';
import Footer from '../../components/Footer';

const ClientLayout = ({
  user,
  routes,
  clientRoutes,
  activeRoute,
  changeActiveRoute,
}) => (
  <div>
    <NavigationBar 
      routes={clientRoutes}
      activeRoute={activeRoute}
      changeActiveRoute={changeActiveRoute}
    />
    {routes}
    <Footer />
  </div>
);

export default ClientLayout;