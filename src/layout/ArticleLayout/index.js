import React from 'react';
import PropTypes from 'prop-types';

import {
  Grid,
  Image,
  Container,
  Divider,
  Segment,
} from 'semantic-ui-react';

import ArticleCard from '../../components/ArticleCard';
import articleType from '../../types/articleType';
import ActivityCard from '../../components/ActivityCard';
import SharingButtons from '../../components/SharingButtons';
import ArticleTitle from '../../components/ArticleTitle';

const ArticleLayout = ({ article }) => (
  <Container>
    <Grid columns={16}>
      <Grid.Column width={16}>
        <span>{article.type} | Oleh: {article.author} | {article.datePublished}</span>
      </Grid.Column>
    </Grid>
    
    <Grid columns="equal">
      <Grid.Column width={16}>
        <ArticleTitle
          type={article.type}
          title={article.title}
          description={article.description}
        />
      </Grid.Column>
    </Grid>

    <Grid columns="equal">
      <Grid.Column width={12}>
        <Image src={article.image} size="huge"/>
      </Grid.Column>
      <Grid.Column width={4}>
        <h1 className="display-3">Terkini &nbsp;</h1>
        <Divider />
        <ActivityCard activity={article} />
        <ActivityCard activity={article} />
        <ActivityCard activity={article} />
      </Grid.Column>
    </Grid>

    <Grid columns="equal">
      <Grid.Column width={12}>
        <article>
          <p>Pada hari Kamis tanggal 12 Juli 2018, sekitar pukul 09.00 Wib, upacara pembukaan Pekan Olah Raga (POR) Hari Bhakti Adhyaksa (HBA) ke-58 tahun 2018 resmi dibuka di halaman upacara Kejaksaan Agung RI, upacara tersebut diikuti oleh kontingen diantaranya kontingen pembinaan, kontingen Intelijen, kontingen Pidum, kontingen Datun, kontingen Pidsus, kontingen Badiklat dan kontingen Kejati DKI Jakarta,serta tidak kalah pentingnya yaitu kontingen Keluarga Besar Purna Adhyaksa (KBPA), kontingen pengurus pusat IAD serta kontingen Forum Wartawan Kejaksaan Agung (Forwaka), juga turut serta dalam upacara pembukaan POR tersebut. Dalam Upacara pembukaan POR HBA tahun 2018 Jaksa Agung RI Bapak H.M PRASETYO sebagai Inspektur Upacara.</p>
          <p>Dalam amanatnya Jaksa Agung RI menyampaikan Bahwa pekan Olahraga ini hendaknya bukan sebuah kompetisi untuk saling mengalahkan, namun menjunjung sportifitas insan Adhyaksa untuk mewujudkan solidaritas dan integritas, selaras dengan tema pembukaan Pekan Olah Raga Hari Bhakti Adhyaksa ke-58 kali ini yaitu “Junjung Tinggi Sportifitas Untuk Mewujudkan Solidaritas dan Integritas”. Berbagai cabang olahraga dipertandingkan dalam rangka memperingati Pekan Olah Raga (POR) Hari Bakti Adhiyaksa (HBA) ke-58 tahun 2018.</p>
          <p>Setelah selesai menyampaikan amanatnya, Jaksa Agung RI, Bapak H.M. Prasetyo, membuka POR HBA tahun 2018 dengan menekan tombol sirine, melepas balon ke udara serta pelepasan burung merpati yang menandakan pembukaan POR HBA 2018 resmi dibuka. Pembukaan POR HBA semakin meriah dengan adanya penampilan merching band dari Sekolah Tinggi ilmu Pelayaran yang ikut memeriahkan pembukaan POR HBA 2018 dan pertandingan sepak bola persahabatan antara panitia HBA 2018 dengan forwaka.</p>
          <p>Kajari Jakut, Bapak ROBERTH M. TACOY, S.H, M.H, beserta seluruh pegawai Kejaksaan Negeri Jakarta Utara turut menghadiri kegiatan tersebut dan tentunya berpartisipasi dalam beraneka macam olah raga yang dipertandingkan.</p>
        </article>
      </Grid.Column>
      <Grid.Column width={4}>
        <h1 className="display-3">Kegiatan &nbsp;</h1>
        <Divider />
        <ActivityCard activity={article} />
        <ActivityCard activity={article} />
        <ActivityCard activity={article} />
      </Grid.Column>
    </Grid>
    <SharingButtons />
    <Divider />
    <Segment basic className="related-article-section">
      <h2 class="display-3">Artikel Terkait</h2>
      <Grid columns="equal">
        <Grid.Column>
          <ArticleCard article={article} vertical/>
        </Grid.Column>

        <Grid.Column>
          <ArticleCard article={article} vertical/>
        </Grid.Column>

        <Grid.Column>
          <ArticleCard article={article} vertical/>
        </Grid.Column>

        <Grid.Column>
          <ArticleCard article={article} vertical/>
        </Grid.Column>
      </Grid>
    </Segment>
  </Container>
);

ArticleLayout.PropTypes = {
  article: PropTypes.shape(articleType).isRequired,
};

export default ArticleLayout;