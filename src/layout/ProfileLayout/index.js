import React from 'react';
import PropTypes from 'prop-types';
// UI Library
import {
  Grid,
  Container,
  Divider,
  Image,
} from 'semantic-ui-react';
// Assets
import logo from '../../assets/images/logo-kejaksaan.png';
// Types
import articleType from '../../types/articleType';
/* Components */
import ActivityCard from '../../components/ActivityCard';
import BackgroundCover from '../../components/BackgroundCover';

const ArticleLayout = ({ article }) => (
  <div style={{marginTop: '-16px'}}>    
    <BackgroundCover image={article.image} />
    <Container>
      <Grid columns="equal">
        <Grid.Column width={16}>
          <h3 className="display-subtitle">Profile:</h3>
          <h1 className="display-title">{article.title}</h1>
        </Grid.Column>
      </Grid>

      <Grid columns="equal">
        <Grid.Column width={10}>
          <article>
            <p>
              Secara Geografis Kejaksaan Negeri Jakarta Utara sebagai Kejaksaan Negeri yang berkedudukan di wilayah daerah Kotamadya Jakarta Utara dan salah satu bagian dari wilayah yang berada di Ibu Kota Negara, memiliki luas wilayah daratan 155,01 kilometer persegi (Km²), dengan jumlah penduduk lebih dari 1.181.096 jiwa, dan Kantor Kejaksaan Negeri Jakarta Utara terletak di Jalan Enggano Nomor 1 Tanjung Priok Kotamadya Jakarta Utara
            </p>
            <p>
              Dalam melaksanakan penegakkan hukum, Kejaksaan tidak dapat bertindak di luar rambu-rambu hukum, Kejaksaan berpedoman pada asas legalitas yang bersifat universal dan mengikat bagi seluruh aparat penegak hukum dalam bertindak. Pembaharuan Kejaksaan dalam aspek organisasi, tata kerja dan sumber daya manusia serta manajemen teknis perkara dan pengawasan merupakan program prioritas yang harus direspon dalam rangka Reformasi Birokrasi guna mendukung tekad pemerintah untuk mewujudkan pemerintahan yang bersih dan berwibawa (clean governance dan good governance).
            </p>
            <p>
              Sejalan dengan hal itu, pemberdayaan sumber daya manusia diperlukan dalam rangka peningkatan kinerja yang profesional, untuk menciptakan sumber daya manusia yang berdaya guna dan berhasil guna. Profesionalisme memerlukan pembenahan dan penguatan elemen dan unsur pendukung, dalam hal ini Jaksa dan seluruh pegawai Kejaksaan termasuk sarana dan prasarana pendukung.
            </p>
            <p>
              Peningkatan sumber daya manusia yang profesional merupakan hal yang sangat strategis, bahkan dapat dikatakan sebagai Condio Sine Qua Non dalam organisasi karena pengaruh yang signifikan dan komprehensif bagi setiap proses aktivitas yang dapat mewujudkan kinerja sebagaimana yang diharapkan.
            </p>
            <p>
              Kinerja yang profesional seorang Jaksa atau pegawai kejaksaan dapat diukur dari hasil yang telah dicapai secara menyeluruh dalam ukuran etik dan profesi. Etik berdasarkan Doktrin Tri Krama Adhyaksa, yang mempunyai nilai-nilai luhur yaitu melaksanakan tugas dengan Kesetiaan, Kejujuran, Bertanggung Jawab dan Bijaksana.
            </p>
          </article>
        </Grid.Column>
        <Grid.Column>
          <Image 
            src={logo}
          />
          <h1 className="display-3">{article.type} &nbsp;</h1>
          <Divider />
          <ActivityCard activity={article} />
          <ActivityCard activity={article} />
          <ActivityCard activity={article} />
        </Grid.Column>
      </Grid>
      <Divider />
    </Container>
  </div>
);

ArticleLayout.PropTypes = {
  article: PropTypes.shape(articleType).isRequired,
};

export default ArticleLayout;