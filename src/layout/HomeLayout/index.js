import React from 'react';
import {
  Grid,
  Divider,
  Icon,
  Container,
  Image,
} from 'semantic-ui-react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';

import LogoKejaksaan from '../../assets/images/logo-kejaksaan.png';
import VerticalMenu from '../../components/VerticalMenu';

const sliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
};

const HomeLayout = ({headlines, articles, activities}) => (
  <div>
    <header className="bg-header">
      <Grid columns="equal" verticalAlign="middle" centered>
        <Grid.Column width={3}>
          <Image src={LogoKejaksaan} size="medium"/>
        </Grid.Column>
        <Grid.Column>
          <h1 className="display-5 bold">Kejaksaan Negeri Tanah Bumbu</h1>
          <span className="subtitle">Kabupaten Tanah Bumbu, Kalimantan Selatan</span>
        </Grid.Column>
      </Grid>
    </header>
    <Grid columns={1}>
      <Grid.Column>
        <Slider {...sliderSettings}>
          {headlines}
        </Slider>
        </Grid.Column>
    </Grid>
    <div className="home-layout">
      <Grid columns="equal" >
        <Grid.Column width={12}>
          <Grid columns="equal">
            <Grid.Column>
              <h1 className="display-3"><Icon name="newspaper outline" /> &nbsp; Info Terkini</h1>
              <Divider/>
              {articles}
              <Link to="/pers">Info Terkini Lainnya &nbsp;<Icon name="chevron right"/></Link>
            </Grid.Column>
          </Grid>

          <Grid columns="equal">
            <Grid.Column>
              <h1 className="display-3">
                Siaran Pers &nbsp;
                <Icon name="bullhorn"/>
              </h1>
              <Divider />
              {activities}
              <Link to="/pers">Siaran Lainnya &nbsp;<Icon name="chevron right"/></Link>
            </Grid.Column>

            <Grid.Column>
              <h1 className="display-3">
                Kerja Nyata &nbsp;
                <Icon name="briefcase"/>
              </h1>
              <Divider />
              {activities}
              <Link to="/pers">Selengkapnya &nbsp;<Icon name="chevron right"/></Link>
            </Grid.Column>
          </Grid>
          

        </Grid.Column>

        <Grid.Column width={4}>
          <Grid columns="equal">
            <Grid.Column>
              <h1 className="display-3"><Icon name="legal"/> &nbsp; Dakwaan</h1>
              <Divider/>
              <VerticalMenu
                listMenu={
                  [
                    { name: 'pidana-umum', text: 'Pidana Umum' },
                    { name: 'pidana-khusus', text: 'Pidana Khusus' },
                  ]
                }
              />
            </Grid.Column>
          </Grid>
          
          <Grid columns="equal">
            <Grid.Column>
              <h1 className="display-3"><Icon name="balance scale"/> &nbsp; Info Perkara</h1>
              <Divider/>
              <VerticalMenu
                listMenu={
                  [
                    { name: 'pidana-umum', text: 'Pidana Umum' },
                    { name: 'pidana-khusus', text: 'Pidana Khusus' },
                    { name: 'perdata', text: 'Perdata, & Tata Usaha Negara' },

                  ]
                }
              />
            </Grid.Column>
          </Grid>

          <Grid columns="equal">
            <Grid.Column>
              <h1 className="display-3"><Icon name="calendar outline"/> &nbsp; Kegiatan</h1>
              <Divider/>
              {activities}
              <Link to="/pers">Kegiatan Lainnya &nbsp;<Icon name="chevron right"/></Link>
            </Grid.Column>
          </Grid>
        </Grid.Column>
      </Grid>
    </div>
  </div>
);

export default HomeLayout;