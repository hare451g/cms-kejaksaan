import React from 'react';
// view library
import {
  Segment,
  Form,
  Button,
  Grid,
  Image,
  Message,
} from 'semantic-ui-react';

// asset
import logo from '../../assets/images/logo-kejaksaan.png';

const LoginLayout = ({
  onInputTextChanged,
  username,
  password,
  onLoginSubmit,
  failAttempt,
  isLoading,
  isError,
}) => (
  <div>
    <Grid className="fill-height" verticalAlign="middle" container>
      <Grid.Column width={8}>
        <h1 align="center">Kejaksaan Republik Indonesia</h1>
        <Image src={logo} size="massive" />
      </Grid.Column>
      <Grid.Column width={8}>
        <h1 className="display-3">Administrator Login</h1>
        <Segment attached>
          <Form>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Email"
                type="email"
                name="username"
                onChange={onInputTextChanged}
                value={username}
                disabled={isLoading}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Password"
                type="password"
                name="password"
                onChange={onInputTextChanged}
                value={password}
                disabled={isLoading}
              />
            </Form.Group>
          </Form>
        </Segment>
        <Button.Group size="large" attached="bottom">
          <Button
            primary
            onClick={onLoginSubmit}
            disabled={failAttempt > 3 || isLoading}
          >
            Login
          </Button>
          <Button.Or></Button.Or>
          <Button
            onClick={onLoginSubmit}
            disabled={failAttempt > 3}
          >
            Forget password
          </Button>
        </Button.Group>
        <br />
        {
          isError ? 
          <Grid>
            <Grid.Column>
              <Message negative>
                <Message.Header>Oops, login failed !</Message.Header>
                <p>{isError.message}</p>
              </Message>
            </Grid.Column>
          </Grid> 
          : 
          <div></div>
        }
      </Grid.Column>
    </Grid>
  </div>
);

export default LoginLayout;