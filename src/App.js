import React, { Component } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';

// routers
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes/index';
import routes from './routes/routes';
import AdminRoutes from './routes/admin';
import adminRoutes from './routes/adminRoutes';

// components
import AdminLayout from './layout/AdminLayout';
import ClientLayout from './layout/ClientLayout';

// firebase
import { logout } from './firebase/auth';
import firebase from './firebase/index';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoute: 'dashboard',
      user: false,
    };
    this.changeActiveRoute = this.changeActiveRoute.bind(this);
  }

  changeActiveRoute(event, {name}) {
    this.setState({activeRoute: name})
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user)=>{
      if (user) {
        this.setState({ user });
      } else {
        this.setState({ user: false })
      }
    })
  }

  render() {
    if (this.state.user) {
      return (
        <Router>
          <div>
            <AdminLayout 
              routes={<AdminRoutes user={this.state.user}/>}
              adminRoutes={adminRoutes}
              activeRoute={this.state.activeRoute}
              changeActiveRoute={this.changeActiveRoute}
            />
          </div>
        </Router>
      );
    }
    return (
      <Router>
        <div>
          <ClientLayout
            routes={<Routes />}
            clientRoutes={routes}
            activeRoute={this.state.activeRoute}
            changeActiveRoute={this.changeActiveRoute}
          />
        </div>
      </Router>
    );
  }
}

export default App;
