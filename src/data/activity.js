import faker from 'faker';
import moment from 'moment';

const generateFakeActivity = new Promise( (resolve, reject) => {
  let fakeActivity = [];
  for(let i = 1; i <= 3; i++ ) {
    const fakeArticle = {
      title: faker.lorem.lines(1),
      description: faker.lorem.paragraph(1),
      datePublished: moment(faker.date.recent(2018)).format('DD-MM-YYYY'),
    }; 
    fakeActivity.push(fakeArticle);
  }
  resolve(fakeActivity);
});

export default generateFakeActivity;