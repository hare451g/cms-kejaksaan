import faker from 'faker';
import moment from 'moment';

const generateFakeHeadlines = new Promise( (resolve, reject) => {
  let fakeArticles = [];
  for(let i = 1; i <= 4; i++ ) {
    const fakeArticle = {
      title: faker.lorem.lines(1),
      description: faker.lorem.paragraph(3),
      content: faker.lorem.paragraphs(4),
      thumbnail: 'http://gnews.online/wp-content/uploads/2016/04/kejatisu.jpg',
      author: faker.name.findName(),
      datePublished: moment(faker.date.past(2018)).format('DD-MM-YYYY'),
    }; 
    fakeArticles.push(fakeArticle);
  }
  resolve(fakeArticles);
});

export default generateFakeHeadlines;