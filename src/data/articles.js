import faker from 'faker';
import moment from 'moment';

const generateFakeArticles = new Promise( (resolve, reject) => {
  let fakeArticles = [];
  for(let i = 0; i <= 3; i++ ) {
    const fakeArticle = {
      title: faker.lorem.lines(1),
      description: faker.lorem.paragraph(1),
      content: faker.lorem.paragraphs(4),
      thumbnail: 'https://images.unsplash.com/photo-1518169811655-27c3a4327016?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d31bd2512f92ca5444a1e501634257ca&auto=format&fit=crop&w=1050&q=80',
      author: faker.name.findName(),
      datePublished: moment(faker.date.recent(2018)).format('DD-MM-YYYY'),
    }; 
    fakeArticles.push(fakeArticle);
  }
  resolve(fakeArticles);
});

export default generateFakeArticles;