import PropTypes from 'prop-types';

const articleType = PropTypes.shape({
  title: PropTypes.string,
  description: PropTypes.string,
  content: PropTypes.string,
  thumbnail: PropTypes.string,
  author: PropTypes.string,
  datePublished: PropTypes.string,
});

export default articleType;
