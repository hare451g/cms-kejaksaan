import PropTypes from 'prop-types';

const routeType = PropTypes.shape({
  name: PropTypes.string,
  text: PropTypes.string,
  path: PropTypes.string,
  component: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  dropdown: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
});

export default routeType;