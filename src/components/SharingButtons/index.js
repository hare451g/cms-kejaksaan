import React from 'react';

import {
  Segment,
  Grid,
} from 'semantic-ui-react';

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailShareButton,
  FacebookIcon,
  LinkedinIcon,
  TwitterIcon,
  WhatsappIcon,
  EmailIcon,
} from 'react-share';

const SharingButtons = () => (
  <Segment basic className="sharing-article-section">
    <Grid columns="equal">
      <Grid.Column width={1}>
        <FacebookShareButton><FacebookIcon round /></FacebookShareButton>
      </Grid.Column>

      <Grid.Column width={1}>
        <TwitterShareButton><TwitterIcon round /></TwitterShareButton>
      </Grid.Column>

      <Grid.Column width={1}>
        <LinkedinShareButton><LinkedinIcon round /></LinkedinShareButton>
      </Grid.Column>

      <Grid.Column width={1}>
        <WhatsappShareButton><WhatsappIcon round /></WhatsappShareButton>
      </Grid.Column>

      <Grid.Column width={1}>
        <EmailShareButton><EmailIcon round /></EmailShareButton>
      </Grid.Column>
    </Grid>
  </Segment>
);

export default SharingButtons;
