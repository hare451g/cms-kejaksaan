import React from 'react';
import moment from 'moment';
import {
  Grid,
  Icon,
  Divider,
  Container,
} from 'semantic-ui-react';

const Footer = () => (
  <footer>
    <Container>
      <Grid columns="equal">
        <Grid.Column>
          <h3>Profil</h3>
          <Divider />
          <ul className="footer-list">
            <li><a href="profil_kejaksaan.php?id=2">Sambutan Jaksa Agung</a></li>
            <li><a href="profil_kejaksaan.php?id=1">Pengertian Kejaksaan</a></li>
            <li><a href="profil_kejaksaan.php?id=3">Sejarah</a></li>
            <li><a href="profil_kejaksaan.php?id=12">Jaksa Agung dari Masa ke Masa</a></li>
            <li><a href="profil_kejaksaan.php?id=4">Logo &amp; Maknanya</a></li>
            <li><a href="profil_kejaksaan.php?id=5">Doktrin Kejaksaan</a></li>
            <li><a href="profil_kejaksaan.php?id=6">Visi &amp; Misi</a></li>
            <li><a href="profil_kejaksaan.php?id=7">Tugas &amp; Wewenang</a></li>
            <li><a href="profil_kejaksaan.php?id=8">Struktur Organisasi</a></li>
            <li><a href="profil_kejaksaan.php?id=9">Profil Pimpinan</a></li>
            <li><a href="profil_kejaksaan.php?id=10">Kontak Kami</a></li>
          </ul>
        </Grid.Column>
        <Grid.Column>
          <h3>Reformasi Birokrasi</h3>
          <Divider />
          <ul className="footer-list">
            <li><a href="reformasi_birokrasi.php?section=8&idkat=6">Quick Wins</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=7">Evaluasi Kinerja</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=8">Profil Kejaksaan 2025</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=9">Manajemen Perubahan</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=10">Analisis Jabatan</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=11">Evaluasi Jabatan</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=12">Sistem&Struktur Remunerasi</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=13">Sistem Manajemen SDM</a></li>
            <li><a href="reformasi_birokrasi.php?section=8&idkat=14">Peraturan Perundang-Undangan</a></li>
          </ul>
        </Grid.Column>
        <Grid.Column>
          <h3>Kontak</h3>
          <Divider />
          <ul className="footer-list">
            <li>
              <Icon name="building outline"/>
              Pusat Penerangan Hukum<br />
              KEJAKSAAN AGUNG R.I<br />
              Jl. Sultan Hasanuddin No.1 Kebayoran Baru<br />
              Jakarta Selatan - Indonesia<br />
            </li>
            <li><Icon name="phone"/> +62-21-722-1269<br /></li>
            <li><Icon name="envelope outline"/>humas.puspenkum@kejaksaan.go.id<br /></li>
          </ul>
        </Grid.Column>
      </Grid>
    </Container>
    <Divider />
    <Container>
      <Grid>
        <Grid.Column>
          Copyright Haredev&copy; {moment().format('YYYY')}
        </Grid.Column>
      </Grid>
    </Container>
  </footer>
);

export default Footer;
