import React from 'react';
import {
  Segment,
} from 'semantic-ui-react';

const BackgroundCover = ({ image }) => (
  <Segment
    className="profile-header"
    style={
      {
        backgroundImage: `url(${image})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }
    }
    basic
  />
);

export default BackgroundCover;
