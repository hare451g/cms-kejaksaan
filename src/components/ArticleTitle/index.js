import React from 'react';
import {
  ARTICLE_TYPE_NEWS,
  ARTICLE_TYPE_FACILITIES,
  ARTICLE_TYPE_PROFILE,
  ARTICLE_TYPE_FIELD,
} from '../../config/constants';

const ArticleTitle = ({type, title, description}) => {
  switch (type) {
    case ARTICLE_TYPE_NEWS:
      return (
        <div>
          <h1 className="display-title">{title}</h1>
          <h4 className="display-subtitle"><i>"{description}"</i></h4>
        </div>
      );

    case ARTICLE_TYPE_FACILITIES:
      return (
        <div>
          <h3 className="display-subtitle">Sarana:</h3>
          <h1 className="display-title">{title}</h1>
        </div>
      );

    case ARTICLE_TYPE_PROFILE:
      return (
        <div>
          <h3 className="display-subtitle">Profile:</h3>
          <h1 className="display-title">{title}</h1>
        </div>
      );

    case ARTICLE_TYPE_FIELD:
      return (
        <div>
          <h3 className="display-subtitle">Bidang:</h3>
          <h1 className="display-title">{title}</h1>
        </div>
      );

    default:
      return (
        <div>
          <h1 className="display-title">{title}</h1>
          <h4 className="display-subtitle"><i>"{description}"</i></h4>
        </div>
      );
  }
};

export default ArticleTitle;
