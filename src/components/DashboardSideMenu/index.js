import React from 'react';
import { Menu, Header, Icon, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { logout } from '../../firebase/auth';

const DashboardSideMenu = ({ routes, activeRoute, onChangeRoute }) => (
  <Menu vertical fluid tabular>
    {
      routes.map(
        route => (
          <Menu.Item
            key={route.name}
            name={route.name}
            active={route.name === activeRoute}
            onClick={onChangeRoute}
          >
            <Header
              as={Link}
              to={route.path}
            >
              <Icon name={route.icon}/>{route.text}
            </Header>
          </Menu.Item>
        )
      )
    }
    <Menu.Item>
      <Header as="a" href="/" onClick={() => logout()}>
        <Icon name="sign out alternate" />Logout
      </Header>
    </Menu.Item>
  </Menu>
);

export default DashboardSideMenu;
