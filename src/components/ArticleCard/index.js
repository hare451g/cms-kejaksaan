import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Image, Icon, Segment } from 'semantic-ui-react';

import articleType from '../../types/articleType';

const ArticleCard = ({ article, vertical, search }) => {
  if (vertical) {
    return (
      <Segment basic className="hover-shadow">
        <Grid verticalAlign="middle" columns="equal">
          <Grid.Column>
            <Image src={article.thumbnail} rounded />
          </Grid.Column>
        </Grid>
        <Grid verticalAlign="middle" columns="equal">
          <Grid.Column>
            <h3>{article.title}</h3>
            <p>{article.description}</p>
            <span className="date"><Icon name="calendar alternate outline"/>{article.datePublished}</span>
          </Grid.Column>
        </Grid>
      </Segment>
    );
  } else if (search) {
    return (
      <Segment basic>
        <Grid verticalAlign="top"  columns="equal">
          <Grid.Column width={3}>
            <h5 align="right" className="date"><Icon name="calendar alternate outline"/>{article.datePublished}</h5>
            <h4 align="right" className="date"><i>{article.type}</i></h4>
          </Grid.Column>
          <Grid.Column>
            <Grid verticalAlign="middle" columns="equal">
              <Grid.Column>
                <Image src={article.thumbnail} size="medium" rounded />
              </Grid.Column>
              <Grid.Column>
                <h3 className="display-2">{article.title}</h3>
                <p>{article.description}</p>
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid>
      </Segment>
    );
  }

  return (
    <Segment basic className="hover-shadow">
      <Grid verticalAlign="middle" columns="equal">
        <Grid.Column>
          <Image src={article.thumbnail} rounded />
        </Grid.Column>
        <Grid.Column>
          <h3>{article.title}</h3>
          <p>{article.description}</p>
          <span className="date"><Icon name="calendar alternate outline"/>{article.datePublished}</span>
        </Grid.Column>
      </Grid>
    </Segment>
  );
};

ArticleCard.propTypes = {
  article: PropTypes.shape(articleType)
}

export default ArticleCard;