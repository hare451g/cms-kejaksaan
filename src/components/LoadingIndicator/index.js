import React from 'react';

import { Dimmer, Loader, Segment } from 'semantic-ui-react'

const LoadingIndicator = ({ message }) => (
  <Segment>
    <Dimmer active>
      <Loader />
      <h3 className="display-2">{message}</h3>
    </Dimmer>
  </Segment>
);

export default LoadingIndicator;
