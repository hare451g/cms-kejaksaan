import React from 'react';
import PropTypes from 'prop-types';
import {
  Menu,
  Dropdown,
  Button,
} from 'semantic-ui-react';

import routeType from '../../types/routeType';
import logo from '../../assets/images/logo-kejaksaan.png';

const AdminNavigationBar = ({username, onLogoutClick}) => (
  <Menu pointing secondary>
    <Menu.Menu position="left">
      <Menu.Item>
        <img src={logo}/> Kejaksaan Republik Indonesia
      </Menu.Item>
    </Menu.Menu>
    <Menu.Menu position="right">
      <Menu.Item
        as={Button}
      >
        {username}
      </Menu.Item>
      <Menu.Item
        onClick={onLogoutClick}
        as={Button}
      >
        Sign Out
      </Menu.Item>
    </Menu.Menu>
  </Menu>
);

AdminNavigationBar.propTypes = {
  routes: PropTypes.oneOfType([
    PropTypes.shape(routeType),
    PropTypes.array,
  ]),
  activeRoute: PropTypes.string.isRequired,
}

export default AdminNavigationBar;
