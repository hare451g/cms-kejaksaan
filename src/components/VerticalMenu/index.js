import React from 'react';
import {Menu, Header, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const VerticalMenu = ({ listMenu }) => (
  <Menu vertical fluid tabular>
    {
      listMenu.map(
        route => (
          <Menu.Item
            key={route.name}
            name={route.name}
          >
            {/* <Header as={Link} to={route.path} >{route.text}</Header> */}
          </Menu.Item>
        )
      )
    }
  </Menu>
);

export default VerticalMenu;
