import React from 'react';
import { Grid, Image, Icon, Segment } from 'semantic-ui-react';

const Headline = ({ headline }) => (
  <Segment basic>
    <Grid verticalAlign="middle" columns="equal">
      <Grid.Column>
        <Image src={headline.thumbnail} size="massive"/>
      </Grid.Column>
      <Grid.Column>
        <h1 className="display-4">{headline.title}</h1>
        <p>{headline.description}</p>
        <span className="date"><Icon name="calendar alternate outline"/>{headline.datePublished}</span>
      </Grid.Column>
    </Grid>
  </Segment>
);

export default Headline;
