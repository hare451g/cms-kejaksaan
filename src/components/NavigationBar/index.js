import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Menu,
  Dropdown,
} from 'semantic-ui-react';

import routeType from '../../types/routeType';
import logo from '../../assets/images/logo-kejaksaan.png';

const NavigationBar = ({routes, activeRoute, changeActiveRoute}) => (
  <Menu pointing secondary>
    <Menu.Menu position="left">
      <Menu.Item>
        <img src={logo}/> Kejaksaan Negeri Tanah Bumbu
      </Menu.Item>
    </Menu.Menu>
    <Menu.Menu position="right">
      {
        routes.map(route => {
          if (route.dropdown) {
            return (
              <Dropdown
                item
                text={route.text}
                key={route.name}
              >
                <Dropdown.Menu>
                  {
                    route.dropdown.map(item => (
                      <Dropdown.Item
                        key={item.name}
                        as={Link}
                        to={item.path}
                      >
                        {item.text}
                      </Dropdown.Item>
                    ))
                  }
                </Dropdown.Menu>
              </Dropdown>
            );
          }
          return (
            <Menu.Item
              key={route.name}
              name={route.name}
              active={activeRoute === route.name}
              onClick={changeActiveRoute}
              as={Link}
              to={route.path}
            >
              {route.text}
            </Menu.Item>
          );
        })
      }
    </Menu.Menu>
  </Menu>
);

NavigationBar.propTypes = {
  routes: PropTypes.oneOfType([
    PropTypes.shape(routeType),
    PropTypes.array,
  ]),
  activeRoute: PropTypes.string.isRequired,
}

export default NavigationBar;
