import React from 'react';
import {
  Segment,
  Icon,
} from 'semantic-ui-react';
const ActivityCard = ({activity}) => (
  <Segment basic className="hover-shadow">
    <h4>{activity.title}</h4>
    <p>{activity.description}</p>
    <span className="date"><Icon name="calendar alternate outline"/>{activity.datePublished}</span>
  </Segment>
);

export default ActivityCard;
