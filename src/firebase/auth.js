import firebase from './index';

const authListener = (() => {
  return firebase.auth().onAuthStateChanged;
});

const getUser = () => localStorage.getItem('user');

const login = ({username, password}) => {
  return firebase.auth().signInWithEmailAndPassword(username, password)
};

const logout = () => {
  firebase.auth().signOut();
};

const signUp = ({username, password}) => {
  alert(`Login attempt with ${username} : ${password}`);
};

export { 
  authListener,
  getUser,
  login,
  logout,
  signUp,
};