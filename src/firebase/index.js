import fire from 'firebase';
import config from './config';

const firebase = fire.initializeApp(config);

export default firebase;