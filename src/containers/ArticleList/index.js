import React from 'react';

import ArticleListLayout from '../../layout/ArticleListLayout';
import LoadingIndicator from '../../components/LoadingIndicator';
import ArticleCard from '../../components/ArticleCard';

class ArticleList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      articles: [
        {
          title: 'Kejaksaan Negeri Tanah Bumbu',
          description: 'Ajang Kompetisi untuk Menjunjung Sportifitas',
          content: `
          `,
          image: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
          thumbnail: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
          author: 'Rukiyamin Sandono',
          datePublished: '10 Agustus 2018, 08:00',
          type: 'Profile',
        },
        {
          title: 'Kejaksaan Negeri Tanah Bumbu',
          description: 'Ajang Kompetisi untuk Menjunjung Sportifitas',
          content: `
          `,
          image: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
          thumbnail: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
          author: 'Rukiyamin Sandono',
          datePublished: '10 Agustus 2018, 08:00',
          type: 'Profile',
        },
        {
          title: 'Kejaksaan Negeri Tanah Bumbu',
          description: 'Ajang Kompetisi untuk Menjunjung Sportifitas',
          content: `
          `,
          image: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
          thumbnail: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
          author: 'Rukiyamin Sandono',
          datePublished: '10 Agustus 2018, 08:00',
          type: 'Profile',
        },
      ],
      articleId: "",
      isLoading: true,
      isLoaded: false,
    }
  }

  componentDidMount() {
    // Load article by Id
    setTimeout(() => {this.setState({isLoading: false})}, 5000)
  }

  render() {
    return (
      <ArticleListLayout
        isLoading={this.state.isLoading}
        articles={
          this.state.articles.map(
            article => <ArticleCard key={article.title}article={article} search/>
          )
        }
      />
    )
  }
}

export default ArticleList;
