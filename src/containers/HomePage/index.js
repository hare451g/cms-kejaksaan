import React from 'react';

import generateFakeArticles from '../../data/articles';
import generateFakeHeadlines from '../../data/headline';
import generateFakeActivity from '../../data/activity';

import ArticleCard from '../../components/ArticleCard';
import Headline from '../../components/Headline';
import ActivityCard from '../../components/ActivityCard';

import HomeLayout from '../../layout/HomeLayout';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: [],
      headlines: [],
      articles: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
    // fetch article
    const context = this;
    generateFakeHeadlines.then(result => {
      context.setState({
        headlines: result,
      })
    })
    .then(() => {
      generateFakeActivity.then(result=> {
        context.setState({
          activities: result,
        });
      })
    })
    .then(() => {
      generateFakeArticles.then(result => {
        context.setState({
          articles: result,
          isLoaded: true,
        })
      })
    })
    .catch(err => {
      context.setState({
        isLoaded: false,
      });
      alert(err);
    });
  }

  render() {
    if(this.state.isLoaded) {
      return (
        <HomeLayout
          articles={
            this.state.articles.map(
              article => <ArticleCard key={article.title} article={article} />
            )
          }
          headlines={
            this.state.headlines.map(
              headline => <Headline key={headline.title} headline={headline} />
            )
          }
          activities={
            this.state.activities.map(
              activity => <ActivityCard key={activity.title} activity={activity} />
            )
          }
        />
      );
    }
    return (<p>This is homepage</p>);
  }
}

export default HomePage;