import React from 'react';
// firebase
import { login, authListener, getUser } from '../../firebase/auth';
// components
import LoginLayout from '../../layout/LoginLayout';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      failAttempt: 0,
      isLoading: false,
      isError: false,
    };
  }
  // events handler
  onLoginSubmit = (e) => {
    e.preventDefault();
    
    this.setState({isLoading: true});

    login({
      username: this.state.username, 
      password: this.state.password,
    }).then((u)=>{
      this.setState({isLoading: false, isError: false});
    }).catch((error)=>{
      alert(error);
      this.setState({isLoading: false, isError: error});
    });
  }

  onInputTextChanged = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onPasswordChanged = (e) => {
    this.setState({ password: e.target.value });
  }

  render() {
    return (
      <LoginLayout 
        onInputTextChanged={this.onInputTextChanged}
        username={this.state.username}
        onPasswordChanged={this.onInputTextChanged}
        password={this.state.password}
        onLoginSubmit={this.onLoginSubmit}
        failAttempt={this.state.failAttempt}
        isLoading={this.state.isLoading}
        isError={this.state.isError}
      />
    );
  }

}

export default LoginPage;
