import React from 'react';

import ProfileLayout from '../../layout/ProfileLayout';
import LoadingIndicator from '../../components/LoadingIndicator';

class ArticlePage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      articleEntity: {
        title: 'Kejaksaan Negeri Tanah Bumbu',
        description: 'Ajang Kompetisi untuk Menjunjung Sportifitas',
        content: `
        `,
        image: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
        thumbnail: 'https://images.unsplash.com/photo-1436450412740-6b988f486c6b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=5deacc4c54f39048fb2cef6350760649',
        author: 'Rukiyamin Sandono',
        datePublished: '10 Agustus 2018, 08:00',
        type: 'Profile',
      },
      articleId: "",
      isLoading: false,
      isLoaded: false,
    }
  }

  componentDidMount() {
    // Load article by Id
    const slug = this.props.match.params.slug;
  }

  render() {
    if (this.state.isLoading) {
      return (
        <LoadingIndicator message="Loading article . . ." />
      );
    } else if (this.state.isLoaded && !this.state.isLoading) {
      return (
        <ProfileLayout 
          article={this.state.articleEntity}
        />
      );
    }
    return (
      <ProfileLayout 
          article={this.state.articleEntity}
        />
    );
  }
}

export default ArticlePage;
