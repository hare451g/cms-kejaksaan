import React from 'react';

import ArticleLayout from '../../layout/ArticleLayout';
import LoadingIndicator from '../../components/LoadingIndicator';

class ArticlePage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      articleEntity: {
        title: 'Pembukaan POR Hari Bhakti Adhyaksa ke-58 Tahun 2018',
        description: 'Ajang Kompetisi untuk Menjunjung Sportifitas',
        content: `
        `,
        image: 'https://www.kejari-jakut.go.id/images/1531337160.JPG',
        thumbnail: 'https://www.kejari-jakut.go.id/images/1531337160.JPG',
        author: 'Rukiyamin Sandono',
        datePublished: '10 Agustus 2018, 08:00',
        type: 'KEGIATAN',
      },
      articleId: "",
      isLoading: false,
      isLoaded: false,
    }
  }

  componentDidMount() {
    // Load article by Id
  }

  render() {
    if (this.state.isLoading) {
      return (
        <LoadingIndicator message="Loading article . . ." />
      );
    } else if (this.state.isLoaded && !this.state.isLoading) {
      return (
        <ArticleLayout 
          article={this.state.articleEntity}
        />
      );
    }
    return (
      <ArticleLayout 
          article={this.state.articleEntity}
      />
    );
  }
}

export default ArticlePage;
